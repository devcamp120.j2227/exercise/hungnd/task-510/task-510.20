
// Khai báo thư viện express
const express = require('express');
// Import Middleware
const {
    getAllVouchersMiddleware,
    getVouchersMiddleware,
    postVouchersMiddleware,
    putVouchersMiddleware,
    deleteVouchersMiddleware
} = require('../middlewares/voucherMiddleware');

// tạo ra Router
const voucherRouter = express.Router();

// Import voucher Controller
const voucherController = require('../controllers/voucherController');

// create new voucher
voucherRouter.post('/vouchers', postVouchersMiddleware, voucherController.createVoucher);

// get all voucher
voucherRouter.get('/vouchers', getAllVouchersMiddleware, voucherController.getAllVoucher);

// get a voucher by id
voucherRouter.get('/vouchers/:voucherId', getVouchersMiddleware, voucherController.getVoucherById);

// update a voucher by id
voucherRouter.put('/vouchers/:voucherId', putVouchersMiddleware, voucherController.updateVoucherById);

// delete a voucher by id
voucherRouter.delete('/vouchers/:voucherId', deleteVouchersMiddleware, voucherController.deleteVoucherById);

module.exports = { voucherRouter };