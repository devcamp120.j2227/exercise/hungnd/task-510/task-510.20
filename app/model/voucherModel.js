// Import thư viện mongoose
const mongoose = require("mongoose");
// class Schema từ thư viện mongoose
const Schema =  mongoose.Schema;

// Khởi tạo instance voucherSchema từ class Schema
const voucherSchema = new Schema({
    maVoucher: {
        type: String,
        unique: true,
        required: true
    },
    phanTramGiamGia: {
        type: Number,
        required: true
    },
    ghiChu: {
        type: String,
        required: false
    }
});
module.exports = mongoose.model("Voucher", voucherSchema);