// khai báo thư viện mongoose
const mongoose = require("mongoose");

// Khai báo models
const userModel = require("../model/userModel");

// Create new user
const createUser = (req, res) => {
    //B1: Thu thập dữ liệu
    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    // Kiểm tra fullName
    if (!body.fullName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "fullName is not valid!"
        });
    }
    // Kiểm tra email
    if (!body.email || !validateEmail(body.email)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Email is not valid!"
        });
    }
    // Kiểm tra address
    if (!body.address) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Address is not valid!"
        });
    }
    // Kiểm tra số điện thoại
    if (!body.phone || !validatePhone(body.phone)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Phone is not valid!"
        });
    }
    //B3: Thực hiện khởi tạo mới user
    const newUser = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone
    }

    userModel.create(newUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new user successfully!",
                "data": data
            })
        }
    })
};
// Get all user
const getAllUser = (req, res) => {
    //B1: Thu thập dữ liệu
    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện load all user
    userModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Load all user successfully!",
                "data": data
            })
        }
    })
};
// Get user by id
const getUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    // B3: Thực hiện load user theo id
    userModel.findById(userId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get user by Id successfully!",
                "data": data
            })
        }
    })
};
// Update user by id
const updateUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);
    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    // KIểm tra user Id
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    // Kiểm tra fullName
    if (!body.fullName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "fullName is not valid!"
        });
    }
    // Kiểm tra email
    if (!body.email || !validateEmail(body.email)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Email is not valid!"
        });
    }
    // Kiểm tra address
    if (!body.address) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Address is not valid!"
        });
    }
    // Kiểm tra số điện thoại
    if (!body.phone || !validatePhone(body.phone)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Phone is not valid!"
        });
    }
    //B3: Thực hiện khởi tạo mới user
    const updateUser = {
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone
    }
    userModel.findByIdAndUpdate(userId, updateUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update user by Id successfully!",
                "data": data
            })
        }
    })
};

// Delete user by id
const deleteUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    userModel.findByIdAndDelete(userId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete user by Id successfully!",
                "data": data
            })
        }
    })
}

// hàm kiểm tra định dạng email
const validateEmail = (paramEmail) => {
    "use strict";
    var atposition = paramEmail.indexOf("@");
    var dotposition = paramEmail.lastIndexOf(".");
    if (atposition < 1 || dotposition < (atposition + 2) || (dotposition + 2) >= paramEmail.length) {
        return false;
    }
    return true;
}
// hàm kiểm tra định dạng số điện thoại
const validatePhone = (number) => {
    "use strict";
    var vform = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;
    if (vform.test(number)) {
        return true;
    }
    else {
        return false;
    }
}
module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}